///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file factory.cpp
/// @version 1.0
///
/// Animal Factory
///
/// @author Christianne Young <clyyoung@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   05 Mar 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <random>

#include "factory.hpp"
#include "animal.hpp"
#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;
using namespace animalfarm;

namespace animalfarm{

Animal *AnimalFactory::getRandomAnimal(){
   Animal* newAnimal = NULL;
   random_device rd;
   int random = rd()%6;
   switch(random){
      case 0:  newAnimal = new Cat (Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender()); 
               break;
      case 1:  newAnimal = new Dog (Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender()); 
               break;
      case 2:  newAnimal = new Nunu (Animal::getRandomBool(), RED, Animal::getRandomGender());
               break;
      case 3:  newAnimal = new Aku (Animal::getRandomWeight(0.0,2.0), SILVER, Animal::getRandomGender());
               break;
      case 4:  newAnimal = new Palila (Animal::getRandomName(), YELLOW, Animal::getRandomGender());
               break;
      case 5:  newAnimal = new Nene (Animal::getRandomName(), BROWN, Animal::getRandomGender());
               break;
   }
   return newAnimal;
}
}
