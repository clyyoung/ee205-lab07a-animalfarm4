///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Christianne Young <clyyoung@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   05 Mar 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <random>

#include "animal.hpp"
#include "factory.hpp"
#include "node.hpp"
#include "list.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;
using namespace animalfarm;

int main() {
   SingleLinkedList animalList; // Instantiate a SingleLinkedList
   
   for( auto i = 0 ; i < 2 ; i++ ) {
      animalList.push_front( AnimalFactory::getRandomAnimal() ) ;
   }
   
   cout << "Is it empty: " << boolalpha << animalList.empty() << endl;
   cout << "Number of elements: " << animalList.size() << endl;
   
   cout << "Elements in list" << endl;
   for( auto animal = animalList.get_first()           // for() initialize
          ; animal != nullptr                          // for() test
          ; animal = animalList.get_next( animal )) { // for() increment
      cout << animal << endl;
   }

   //cout << "
/*   cout << animalList.pop_front() << endl;
   cout << animalList.pop_front() << endl;
   cout << animalList.pop_front() << endl;
   cout << animalList.get_first() << endl;
   cout << animalList.empty() << endl;
*/
   while( !animalList.empty() ) {
      Node* animal = animalList.pop_front();
      cout << "Animal ptr = " << animal << endl;
      delete (Animal*) animal;
   }

   
   return 0;
}
