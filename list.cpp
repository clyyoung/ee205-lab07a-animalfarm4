///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// File for List class
///
/// @file list.cpp
/// @version 1.0
///
/// @author Christianne Young <clyyoung@hawaii.edu>
/// @brief  Lab 07a - Animal Farm 4 - EE 205 - Spr 2021
/// @date   23 Mar 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <stdlib.h>
#include "list.hpp"

using namespace std;

namespace animalfarm{

const bool SingleLinkedList::empty() const{
   return head == nullptr;
};

void SingleLinkedList::push_front(Node* newNode){
   newNode->next = head;
   head = newNode;
};

Node* SingleLinkedList::pop_front(){
   if(head == nullptr)
      return nullptr;
   Node* temp = head;
   head = temp->next;
   return temp;
};

Node* SingleLinkedList::get_first() const{
   return head;
};

Node* SingleLinkedList::get_next(const Node* currentNode) const{
   Node* node = head;
   while(node != currentNode){
      node = node->next;
   }
   return node->next;
};

size_t SingleLinkedList::size() const{
   size_t i = 0;
   Node* node = head;
   while(node != nullptr){
      i++;
      node = node->next;
   }
   return i;
};

}
