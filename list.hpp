///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// Header file for List class
///
/// @file list.hpp
/// @version 1.0
///
/// @author Christianne Young <clyyoung@hawaii.edu>
/// @brief  Lab 07a - Animal Farm 4 - EE 205 - Spr 2021
/// @date   23 Mar 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "node.hpp"

namespace animalfarm {

class SingleLinkedList {
protected:
   Node* head = nullptr;

public:
   const bool  empty() const;
   void        push_front( Node* newNode );
   Node*       pop_front() ;
   Node*       get_first() const;
   Node*       get_next( const Node* currentNode ) const;
   size_t      size() const;
};

}
